const express = require('express')
const bodyParser = require('body-parser')
const graphqlHttp = require('express-graphql')
const { buildSchema } = require('graphql')
const mongoose = require('mongoose')

const Event = require('./models/Event')

const app = express()

const events = []

app.use(bodyParser.json())

// Mutation --> Change data
// Query --> Fetch data
app.use('/graphql', graphqlHttp({
  schema: buildSchema(`
    type Event {
      _id: ID!
      title: String!
      description: String!
      price: Float!
      date: String!
    }
    input EventInput {
      title: String!
      description: String!
      price: Float!
      date: String!
    }
    type RootQuery {
      events: [Event!]!
    }
    type RootMutation {
      createEvent(eventInput: EventInput): String
    }
    schema {
      query: RootQuery
      mutation: RootMutation
    }  
  `),
  rootValue: {
    events: () => {
      return events
    },
    createEvent: args => {
      const event = {
        _id: Math.random().toString(),
        title: args.eventInput.title,
        description: args.eventInput.description,
        price: +args.eventInput.price,
        date: args.eventInput.date
      }
      events.push(event)
      return event
    }
  },
  // NOTE: Don´t forget to change to false when deploy.
  graphiql: true
}))

mongoose
  .connect(`mongodb://${process.env.DATABASE_SERVER}/${process.env.DATABASE}`, {useNewUrlParser: true})
  .then(() => {
      app.listen(3000)
  })
  .catch(err => {
    console.log(err)
  })
